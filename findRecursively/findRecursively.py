from pathlib import Path

def iterateOverFiles(
    pathFiles: str
) -> None:
    """Function that allows to iterate over filePaths Array"""
    keyword = "test"
    for file in pathFiles:
        findInFile(file, keyword)

def findInFile(
    file: str,
    keyword: str
) -> None:
    """Function that allows you to search a word in a file path and prints the line where it appears."""
    with open(r'{0}'.format(file), 'r') as file:
        for line in file:
            if keyword in line:
                print("found in: ",file.name)
                print(line)

## MAIN ## 
if __name__ == "__main__":
    # Array to save PathFiles
    pathFiles = []
    # Path name for every path in pathFiles array
    for path in Path('./').glob('**/*.*'):
        # Append to Array
        pathFiles.append(path.resolve())

    iterateOverFiles(pathFiles)